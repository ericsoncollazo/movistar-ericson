describe('Search Movistar',function(){
    beforeEach(function(){
    cy.visit('https://tienda.movistar.com.ar/');
    })
it('CP001 - Validar cuotas en compra de equipo -Cuotas.12 -Equipo.A52',function(){
    cy.get('#search_mini_form > .actions').click();
    cy.get('#search').type('A52');
    cy.get('.action.search').click();
    cy.get('form > .action').click();
    cy.get('#installments-text').contains('12 cuotas');
        })
it('CP002 - Aplicar filtro de equipos -Gama.Alta -Memoria Interna.256GB',function(){
    cy.get('#layered-filter-block > .block-title > strong').click();
    cy.get('[attribute="movistar_internalmemory"] > .filter-options-content > .items > :nth-child(3) > a').click();
    it('Cantidad de equipos filtrados',function(){
        cy.expect('.toolbar-number'==5).to.be.true;
    })
    it('El filtro devuelve productos',function(){
        cy.expect('.wrapper > .products').to.exist;
    })
})
it('CP003 - Validar cuotas en compra de equipo -Cuotas.60 -Equipo.Tercero de la lista -Banco.Credicoop -Tarjeta.Visa',function(){
    cy.get('.product-image-wrapper').eq(2).click();
    cy.get('#open-installments-modal').click();
    cy.expect('#installments-text'=='60 cuotas').to.be.false;
})
it('CP004 ordenar equipos por marca y tamaño',function(){
    cy.get('#layered-filter-block-container > .toolbar > .toolbar-sorter > #sorter').select('Marca');
    cy.get('#layered-filter-block > .block-title > strong').click();
    cy.get('[attribute="movistar_screensize"] > .filter-options-content > .items > :nth-child(5) > a').click();
     })
})